<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2018/12/31
 * | ---------------------------------------------------------------------------------------------------
 * | Desc: 多进程示例，并发
 * | ---------------------------------------------------------------------------------------------------
 * | document: https://wiki.swoole.com/wiki/page/214.html
 * | ---------------------------------------------------------------------------------------------------
 */

echo "curl_start_time: " . date( 'Ymd H:i:s' ) . PHP_EOL;

$workers = array();

$urls = array(
    'http://www.baidu.com/?s=johnxu',
    'http://www.baidu.com/?s=peter',
    'http://www.baidu.com/?s=tom',
    'http://news.sina.com.cn',
    'http://qq.com',
    'http://www.johnxu.net'
);

for ( $i = 0; $i < count( $urls ); $i++ )
{
    $process = new \Swoole\Process( function ( \Swoole\Process $process ) use ( $i, $urls ) {
        getUrl( $urls[$i] );
    }, true );

    $process->start();
    $workers[$i] = $process;
}

// 打印管道内的内容
foreach ( $workers as $worker )
{
    echo $worker->read() . PHP_EOL;
}

function getUrl( $url )
{
    echo "{$url} finished \r\n";
    sleep( 1 );
}

echo "curl_end_time: " . date( 'Ymd H:i:s' ) . PHP_EOL; // 并行执行：最终执行时间1S一下
