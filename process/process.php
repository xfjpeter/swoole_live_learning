<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2018/12/31
 * | ---------------------------------------------------------------------------------------------------
 * | Desc: 進程間通信
 * | ---------------------------------------------------------------------------------------------------
 */

/**
 * redirect_stdin_stdout: false打印子進程數據到屏幕，true不打印
 * ps -aux | grep process.php 查看主進程號
 * pstree -p 主進程號  查看子進程號
 */


$process = new \Swoole\Process( function ( \Swoole\Process $process ) {
    $process->exec( '/usr/local/php/bin/php', array( '/home/wwwroot/johnxu.swoole.local.cn/server/http.php' ) );
}, false );

$pid = $process->start(); // 創建子進程

echo $pid . PHP_EOL;

$result = \Swoole\Process::wait();

var_dump( $result );
