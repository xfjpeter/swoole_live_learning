<?php

/**
 * TCP 客户端
 */

$client = new swoole_client( SWOOLE_SOCK_TCP );

$flag = $client->connect( '127.0.0.1', 9501 );

if ( !$flag )
{
    exit( '连接服务器失败' );
}
fwrite( STDOUT, '请输入要发送的内容' ); // 控制台提示用户输入消息
$data = fgets( STDIN ); // 接收到用户输入的内容
$flag = $client->send( $data ); // 发送数据到服务器
if ( !$flag )
{
    exit( '消息发送失败' );
}

$msg = $client->recv(); // 接收服务器返回的内容
var_dump( $msg );

// $client->close();
