<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2019/1/1
 * | ---------------------------------------------------------------------------------------------------
 * | Desc: 经纬度距离计算器
 * | ---------------------------------------------------------------------------------------------------
 */

/**
 * 计算两个坐标的距离
 *
 * @param $lngStart
 * @param $latStart
 * @param $lngEnd
 * @param $latEnd
 *
 * @return float
 */
function distance( $lngStart, $latStart, $lngEnd, $latEnd )
{
    $distance = sin( deg2rad( $lngStart ) ) * sin( deg2rad( $lngEnd ) ) * cos( deg2rad( $latStart ) ) * cos( deg2rad( $latEnd ) ) * cos( deg2rad( $lngStart - $lngEnd ) );

    return rad2deg( acos( $distance ) ) * 69.09;
}

$lngStart = '104.0536984505854';
$latStart = '30.22706387306575';
$lngEnd   = '104.065078';
$latEnd   = '30.256129';


var_dump( distance( $lngStart, $latStart, $lngEnd, $latEnd ) );
