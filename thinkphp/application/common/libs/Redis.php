<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2019/1/2
 * | ---------------------------------------------------------------------------------------------------
 * | Desc: Redis封装，不能读取thinkphp框架内容
 * | ---------------------------------------------------------------------------------------------------
 */

namespace app\common\libs;

class Redis
{
    private static $instance = null;
    private        $redis    = null;
    private        $prefix   = 'sms_';

    /**
     * Redis constructor.
     */
    private function __construct()
    {
        if ( is_null( $this->redis ) )
        {
            $this->redis = new \Redis();
            $this->redis->connect( '127.0.0.1', 6379 );
            $this->redis->auth( '123456' );
        }
    }

    /**
     * Get Instance
     *
     * @return Redis|null
     */
    public static function getInstance()
    {
        if ( is_null( self::$instance ) )
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * 设置验证码的值
     *
     * @param string $phone
     * @param string $code
     * @param int    $expires
     */
    public function setSms( $phone, $code, $expires = 120 )
    {
        $this->redis->set( $this->prefix . $phone, $code, $expires );
    }

    /**
     * 获取验证码
     *
     * @param $phone
     *
     * @return bool|string
     */
    public function getSms( $phone )
    {
        return $this->redis->get( $this->prefix . $phone );
    }
}
