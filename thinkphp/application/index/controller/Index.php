<?php

namespace app\index\controller;

use app\common\libs\Redis;
use johnxu\tool\Restful;

class Index extends Restful
{

    public function index()
    {
        return 'johnxu-swool-study.' . mt_rand( 1000, 9999 );
    }

    public function test( $name = 'ThinkPHP' )
    {
        return 'hello ' . $name;
    }

    /**
     * 登录
     */
    public function login()
    {
        $phoneNumber = $_POST['phone_number'];
        $code        = $_POST['code'];

        // TODO: 判断数据格式是否正确

        // 判断验证码是否正确
        $tmpCode = Redis::getInstance()->getSms( $phoneNumber );
        if ( $code == $tmpCode )
        {
            // TODO: 登录成功会返回用户的基本信息
            $this->ok( 200, [ 'message' => '登录成功', 'user' => 'tom' ] );
        }
        else
        {
            $this->fail( 201, '1002', '验证码错误' );
        }
    }
}
