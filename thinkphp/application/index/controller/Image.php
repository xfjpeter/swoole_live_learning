<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2019/1/3
 * | ---------------------------------------------------------------------------------------------------
 * | Desc:
 * | ---------------------------------------------------------------------------------------------------
 */

namespace app\index\controller;

use johnxu\tool\Restful;
use think\Request;

class Image extends Restful
{
    private $domain = 'http://johnxu.swoole.local.cn:8888/uploads/';

    /**
     * 上传图片
     *
     * @param Request $request
     */
    public function upload( Request $request )
    {
        $file = $request->file( 'file' );
        $info = $file->move( '../public/static/uploads/' );

        if ( $info )
        {
            $this->ok( 200, [ 'path' => $this->domain . $info->getSaveName() ] );
        }
        else
        {
            $this->fail( 201, 1003, '上传失败' );
        }
    }
}
