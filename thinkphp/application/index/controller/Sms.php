<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2019/1/2
 * | ---------------------------------------------------------------------------------------------------
 * | Desc: 发送短信验证码
 * | ---------------------------------------------------------------------------------------------------
 */

namespace app\index\controller;

use app\common\libs\Redis;
use johnxu\tool\Restful;

class Sms extends Restful
{
    /**
     * 发送验证码
     */
    public function send()
    {
        $phone = $_GET['phone_number'];

        if ( !preg_match( '/1[34578]{1}[\d]{9}/', $phone ) )
        {
            $this->fail( 201, 1001, '手机号码不正确' );
        }
        else
        {
            // 进行验证码的发送
            $code = mt_rand( 1000, 9999 );
            // Redis::getInstance()->setSms( $phone, $code );

            // 丢到队列中执行
            $_POST['http_server']->task( array(
                'method' => 'sendSms',
                'data'   => array(
                    'phone' => $phone,
                    'code'  => $code
                )
            ) );

            $this->ok( 200, [ 'code' => $code, 'message' => '验证码发送成功' ] );
        }
    }
}
