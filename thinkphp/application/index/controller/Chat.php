<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2019/1/7
 * | ---------------------------------------------------------------------------------------------------
 * | Desc:
 * | ---------------------------------------------------------------------------------------------------
 */

namespace app\index\controller;

use johnxu\tool\Restful;

class Chat extends Restful
{
    /**
     * 发送消息
     */
    public function push()
    {
        $content = $_POST['content'];
        $user    = 'user-' . mt_rand( 1000, 9999 );
        // TODO: 推送给所有人
        foreach ( $_POST['http_server']->ports[1]->connections as $fd )
        {
            $_POST['http_server']->push( $fd, json_encode( array( 'user' => $user, 'content' => $content ) ) );
        }

        $this->ok( 200, array( 'user' => $user, 'content' => $content ) );
    }
}
