#!/usr/bin/bash

# 平滑重启swoole

echo 'starting reload...'

# 根据进程名称查询进程号
pid=`pidof live-master`

# 平滑重启进程
kill -USR1 $pid

echo 'reload success'
