<?php

// 监听8822端口是否处于开启状态

/**
 * 每隔2S监听8822端口是否处于开启状态，如果没有进行报警
 * nohup php listen.php > listen.log & 启动服务
 */
swoole_timer_tick( 2000, function ( $timer_id ) {
    $shell = 'netstat -anp 2>/dev/null | grep 8822 | grep LISTEN | wc -l';

    $result = shell_exec( $shell );

    if ( $result != 1 )
    {
        //TODO: 报警，进程被干掉了(邮箱，短信等方式发送到相关人员那儿)
        echo "error: " . date( 'Ymd H:i:s' ) . PHP_EOL;
    }
    else
    {
        echo "success: " . date( 'Ymd H:i:s' ) . PHP_EOL;
    }
} );
