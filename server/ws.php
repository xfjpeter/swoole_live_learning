<?php

/**
 * websocket 服务
 */

$ws = new swoole_websocket_server( '0.0.0.0', 9501 );

$ws->set( [
    'log_level'             => 5,
    'enable_static_handler' => true,
    'document_root'         => '/home/wwwroot/johnxu.swoole.local.cn/data'
] );

$ws->on( 'open', function ( swoole_websocket_server $server, swoole_http_request $request ) {
    // 可以通过$request->fd获取用户标识
    var_dump( $request );
} );

/**
 * @property $fd       客户端的socket id，使用$server->push推送数据时需要用到
 * @property $data     数据内容，可以是文本内容也可以是二进制数据，可以通过opcode的值来判断
 * @property $opcode   WebSocket的OpCode类型，可以参考WebSocket协议标准文档
 * @property $finish   表示数据帧是否完整，一个WebSocket请求可能会分成多个数据帧进行发送
 */
$ws->on( 'message', function ( swoole_websocket_server $server, swoole_websocket_frame $frame ) {
    var_dump( $frame->data, $frame->opcode, $frame->fd, $frame->finish );
    $server->push( $frame->fd, "我接收到的消息是：{$frame->data}" );
} );

$ws->on( 'close', function ( swoole_websocket_server $server, $fd ) {
    var_dump( "{$fd} 断开连接" );
} );


$ws->start();
