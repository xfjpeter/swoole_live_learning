<?php

/**
 * TCP 服务器端
 */

$tcp = new swoole_server( '127.0.0.1', 9501 );

$tcp->set( array(
    'worker_num'  => 2, // 利用 ps -aft | grep tcp.php 可以查看进程数量
    'max_request' => 1000,
    'log_level'   => 5
) );

/**
 * @param swoole_server $server 服务器操作句柄
 * @param int           $fd     连接的客户端编号
 * @param reactor_id 进程号
 */
$tcp->on( 'connect', function ( $server, $fd, $reactor_id ) {
    var_dump( "{$fd} Connect server, reactor_id is : {$reactor_id}\r\n" );
} );

$tcp->on( 'receive', function ( $server, $fd, $reactor_id, $data ) {
    $server->send( $fd, "接收到的数据是：{$data}, 来自于：{$fd}, reactor_id是： {$reactor_id}" );
} );

$tcp->on( 'close', function ( $server, $fd ) {
    var_dump( "{$fd} 断开了连接\r\n" );
} );

$tcp->start();
