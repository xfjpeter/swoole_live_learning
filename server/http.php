<?php

/**
 * 创建一个http服务器，类似于Apache和Nginx
 */

$http = new swoole_http_server( '0.0.0.0', 9501 );

$http->set( [
    'log_level'             => 5,
    'enable_static_handler' => true,
    'document_root'         => '/home/wwwroot/johnxu.swoole.local.cn/data',
    'worker_num'            => 2
] );


$http->on( 'request', function ( swoole_http_request $request, swoole_http_response $response ) {
    $request->get;
    $response->end( 'Welcome to swoole http server.' );
} );

$http->on( 'close', function () {
    var_dump( 'closed' );
} );

$http->start();
