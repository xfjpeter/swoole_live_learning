<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2019/1/1
 * | ---------------------------------------------------------------------------------------------------
 * | Desc: 携程redis
 * | ---------------------------------------------------------------------------------------------------
 */

/*$redis = new \Swoole\Coroutine\Redis();

$redis->connect( '127.0.0.1', 6379 );

$redis->get( 'imooc' );*/

$http = new \Swoole\Http\Server( '0.0.0.0', 9501 );

$http->on( 'request', 'onRequest' );


function onRequest( \Swoole\Http\Request $request, \Swoole\Http\Response $response )
{
    // 同步操作redis
    $redis = new \Swoole\Coroutine\Redis();
    $redis->connect( '127.0.0.1', 6379 );

    $redis->auth( '123456' );

    $result = $redis->get( 'imooc' );

    $response->header( 'Content-Type', 'text/plain' );

    $response->end( $result );

}

$http->start();
