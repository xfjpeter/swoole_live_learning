<?php
/**
 * | ---------------------------------------------------------------------------------------------------
 * | Author：johnxu <fsyzxz@163.com>.
 * | ---------------------------------------------------------------------------------------------------
 * | Home: https://www.johnxu.net.
 * | ---------------------------------------------------------------------------------------------------
 * | Data: 2018/12/31
 * | ---------------------------------------------------------------------------------------------------
 * | Desc: 异步读取文件
 * | ---------------------------------------------------------------------------------------------------
 */

// 读取文件大小有限制：4M。大于4M的需要用swoole_async_read();
// 文档：https://wiki.swoole.com/wiki/page/184.html

swoole_async_set( array(
    'log_level' => 5
) );

swoole_async_readfile( './1.txt', function ( $filename, $fileContent ) {
    var_dump( "filename: {$filename} \r\n" );

    var_dump( "fileContent: {$fileContent}" );
} );

// 与上面一样的
\Swoole\Async::readfile( './1.txt', function ( $filename, $fileContent ) {
    var_dump( "filename: {$filename} \r\n" );

    var_dump( "fileContent: {$fileContent}" );
} );

echo 'start' . PHP_EOL;
